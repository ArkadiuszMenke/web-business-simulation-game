<?php
	//Connecting to database
	$host = "localhost";
	$user = "root";
	$pass = "";
	$dbname = "usersdb";

	$conn = mysqli_connect($host, $user, $pass, $dbname) or die("Connection Failed!");

	session_start();
	$Email = $_SESSION['emailval'];
	$ServerCode = $_SESSION['servercode'];
	//Removing server and email codes from session data
	unset($_SESSION['emailval']);  
	unset($_SESSION['servercode']); 
	session_destroy();

	$UserCode = $_POST['code'];
	$Password = $_POST['newpassword'];

	define("MODULO", 2484326083);
	define("PRIME", 2058806609);
	define("SaltString", "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");

	//Hashing new password
	function ServerHash($Password)
	{
		$AsciiTot = 0;
		
		for ($i = 0; $i < strlen($Password); $i++)
		{	
			$AsciiTot += ord($Password[$i]);

		}
		
		$AsciiTot = $AsciiTot * PRIME;

		$HashedValue = fmod($AsciiTot,MODULO);
		
		return $HashedValue;	
	}
	//Creating a new salt for password
	function CreateSalt($Password)
	{
		$Salt = "";
		$SaltLen = 64-strlen($Password);
		$RandChar = 0;

		for ($i = 0; $i < $SaltLen; $i++)
		{
			$RandChar = random_int(0,(strlen(SaltString)-1));
			$Salt = $Salt.SaltString[$RandChar];
		}

		return $Salt;

	}

	if ($UserCode != $ServerCode)
	{
		echo "Incorrect code entered";
		header ("refresh:3; url=index.html");
	}
	else
	{
		//Double hashed password
		$Salt = CreateSalt($Password);
		$Password = $Salt.$Password;
		$Password = dechex(ServerHash($Password));

		//Query updating users password
		$sql = "UPDATE logins_tbl SET Password = '$Password', Salt = '$Salt' WHERE Email = '$Email'";

		$result = $conn->query($sql);
				
		if(!mysqli_query($conn,$sql))
		{
			echo "Update error!";
			header ("refresh:3; url=index.html");
		}
		else
		{
			echo "Update successful!";
			header ("refresh:3; url=index.html");
		}
	}



	$conn->close();
?>