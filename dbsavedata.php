<html>
	<head>
		<style>

		body
		{
			background: #282828;
		}

		h1, h2
		{
			margin: 0 auto;
			width:300px;
			color:#ff8c00;
		}

		section
		{
			margin: 0 auto;
			width:300px;
		}

		input
		{
			width:200px;
			color:#ff8c00;
			background:#282828;
			border-color:#ff8c00;
			padding:10px 10px;
		}
		</style>
	</head>
	<body>
		<script>
			//Used to tell simulation to load data rather than start a fresh simulation
			sessionStorage.setItem("loadtype", "LOAD");	
		</script>
	</body>

<?php

	//Connecting to my database (MYSQL included in XAMPP)
	$host = "localhost";
	$user = "root";
	$pass = "";
	$dbname = "usersdb";

	$conn = mysqli_connect($host, $user, $pass, $dbname) or die("Connection Failed!");

	//Opening session storage
	session_start();
	//Setting time zone to Europe, London
	date_default_timezone_set("Europe/London");

	$Username = $_POST["usernamesave"];
	$SaveData = $_POST["playersavedata"];
	$Date = date("Y-m-d H:i:s");

	//Checking if score qualifies for leaderboard
	function LeaderboardCheck($conn,$UserID,$Date,$Score,$SaveData,$LoadData) 
	{

		//Getting save ID
		$sql = "SELECT ID FROM saves_tbl WHERE UserID = '$UserID' AND Date = '$Date'";

		$result = $conn->query($sql);

		if ($result->num_rows > 0)
		{
			while($row = $result->fetch_assoc())
			{
				$SaveID = $row["ID"];
				
			}

			//Checking for user in scores_tbl table (leaderboard storage)
			$sql = "SELECT GameWeeks FROM scores_tbl WHERE UserID = '$UserID'";

			$result = $conn->query($sql);

			if ($result->num_rows > 0)
			{
				while($row = $result->fetch_assoc())
				{
					$CurrentScore = $row["GameWeeks"];
				}
				//Compare both scores and see in new score is better
				if ($Score < $CurrentScore)
				{
					//If score is better than highscore update leaderboard
					$sql = "UPDATE scores_tbl SET SaveID = '$SaveID', GameWeeks = '$Score' WHERE UserID = '$UserID'";

					if(!mysqli_query($conn,$sql))
					{
						echo "Saving Error!";
						$LoadData = json_encode($SaveData);
						$_SESSION['savedata'] = $LoadData;
						header ("refresh:1 url=simulation.php");
					}
					else
					{
						echo "Highscore updated on leaderboard!";
						$LoadData = json_encode($SaveData);
						$_SESSION['savedata'] = $LoadData;
						header ("refresh:1 url=simulation.php");

					}

				}
				else
				{
					echo "Score not better than highscore!";
					$LoadData = json_encode($SaveData);
					$_SESSION['savedata'] = $LoadData;
					header ("refresh:1 url=simulation.php");
				}
			}
			else
			{
				//Query adding score onto into leaderboard table
				$sql = "INSERT INTO scores_tbl (UserID,SaveID,GameWeeks) VALUES ('$UserID','$SaveID','$Score')";

				if(!mysqli_query($conn,$sql))
				{
					echo "Leaderboard error!";
					$LoadData = json_encode($SaveData);
					$_SESSION['savedata'] = $LoadData;
					header ("refresh:1 url=simulation.php");
				}
				else
				{
					echo "You are now on the Leaderboard!";
					$LoadData = json_encode($SaveData);
					$_SESSION['savedata'] = $LoadData;
					header ("refresh:1 url=simulation.php");
				}
			}
		}
		else
		{
			
		}

		
	}

	//Player score in weeks
	$Score = $_POST['finaluserscore'];


	//Query fetches UserID to reference in database
	$sql = "SELECT ID FROM `logins_tbl` WHERE Username = '$Username'";

	$result = $conn->query($sql);

	if ($result->num_rows > 0)
	{
		while($row = $result->fetch_assoc())
		{
			$UserID = $row["ID"];
			
		}
	}
	else
	{
		echo "User could not be identified";
	}

	//Query loads player data to count number of current saves
	$SaveCount = 0;

	$sql = "SELECT SaveData FROM saves_tbl WHERE UserID = '$UserID'";

	$result = $conn->query($sql);

	if ($result->num_rows > 0)
	{
		while($row = $result->fetch_assoc())
		{
			$SaveCount += 1;
		}
	}
	else
	{
		
	}

	//Checks if player has 3 saves (MAX)
	if ($SaveCount == 3)
	{	

		//List to store timestamps for user reference when overwriting file
		$ListOfTimestamps = array();

		$sql = "SELECT saves_tbl.Date FROM saves_tbl INNER JOIN logins_tbl ON saves_tbl.UserID = logins_tbl.ID WHERE logins_tbl.Username = '$Username'";

		$result = $conn->query($sql);

		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) 
		{
			array_push($ListOfTimestamps,$row['Date']);
		}
	
		//Variables used to insert into HTML through echo's
		$LoopCount = 1;
		
		//Formulating page
		echo "<h2>Too many saves stored.</h2>";
		echo "<br>";
		echo "<h2>Overwrite Save?<h2>";
		echo "<section>";
		echo "<br>";
		//Iterating through the 3 saves
        for ($i = 0; $i < 3; $i++)
		{
			//Form names
			$OverwriteForm = "OverwriteForm";
			$NotOverwriteForm = "NotOverwriteForm";

			//Destination documents
			$OverwriteDocDestination = "dbupdatedata.php";
			$NotOverwriteDocDestination = "simulation.php";

			//Method of sending data
			$Method = "post";

			//Form types
			$TypeHidden = "hidden";
			$TypeText = "text";
			$TypeSubmit = "submit";
			
			//Names
			$UserIDName = "userid";
			$SaveDataName = "savedata";
			$TimestampDateName = "timestampdate";
			$TimestampTimeName = "timestamptime";
			$OverwriteSaveDateName = "overwritedate";
			$OverwriteSaveTimeName = "overwritetime";
			$ScoreTransferName = "score";

			//Values
			//Splitting strings for transfer
			$TimestampParts = explode(" ", $Date);
			$OverwriteSaveTimestamp = $ListOfTimestamps[$LoopCount - 1];
			$OverwriteTimestampParts = explode(" " , $OverwriteSaveTimestamp);
			$ScoreTransferValue = $Score;
			
			$CancelOverwriteValue = "Cancel";
			

			//Form creation
			echo "<form name=$OverwriteForm action=$OverwriteDocDestination method=$Method>";
			//Username value
			echo "<input type=$TypeHidden name=$UserIDName value=$UserID>";
			//Save data value
			echo "<input type=$TypeHidden name=$SaveDataName value=$SaveData>";
			//Current timestamp value
			echo "<input type=$TypeHidden name=$TimestampDateName value=$TimestampParts[0]>";
			echo "<input type=$TypeHidden name=$TimestampTimeName value=$TimestampParts[1]>";
			//Overwrite timestamp value
			echo "<input type=$TypeHidden name=$OverwriteSaveDateName value=$OverwriteTimestampParts[0]>";
			echo "<input type=$TypeHidden name=$OverwriteSaveTimeName value=$OverwriteTimestampParts[1]>";
			//Score transfer
			echo "<input type=$TypeHidden name=$ScoreTransferName value=$ScoreTransferValue>";
			//Submit
			echo "<input type=$TypeSubmit value=$OverwriteTimestampParts[0],$OverwriteTimestampParts[1]>";
			echo "</form>";
			//Loop counter for the 3 saves
			$LoopCount += 1;
		}

		//Cancel overwrite

		//Reloading game data before save
		$LoadData = json_encode($SaveData);
		$_SESSION['savedata'] = $LoadData;

		//Form to go back to simulation
		echo "<form name=$NotOverwriteForm action=$NotOverwriteDocDestination method=$Method>";
		echo "<input type=$TypeHidden value=$SaveData>";
		echo "<input type=$TypeSubmit value=$CancelOverwriteValue>";

		echo "</section>";
		
	}
	else
	{

		//Query that adds save data to the saves_tbl table in the database
		$sql = "INSERT INTO `saves_tbl` (`ID`, `UserID`, `SaveData`, `Date`) VALUES (NULL, '$UserID', '$SaveData', '$Date');";

		if(!mysqli_query($conn,$sql))
		{
			$LoadData = json_encode($SaveData);
			$_SESSION['savedata'] = $LoadData;
			header ("refresh:1 url=simulation.php");
		}
		else
		{
			echo "<h1>Saved<h1>";
			$LoadData = json_encode($SaveData);
			$_SESSION['savedata'] = $LoadData;
			header ("refresh:1 url=simulation.php");
		}
	}
			
	//Checking if save qualifies for leaderboard (needs to be final save of that game)
	try
	{
		
		if ($Score != "")
		{
				LeaderboardCheck($conn,$UserID,$Date,$Score,$SaveData,$LoadData);	
		}
		else
		{
			throw new Exception();
		}
	}
	catch (Exception $e)
	{

	}


	
	



	$conn->close();
?>