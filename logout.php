<?php
	//Opeining and terminating active sessions
	session_start();
	echo "Logging out...";
	if(isset($_SESSION['logged_in']))
	{
		unset($_SESSION['logged_in']);  
		session_destroy();
	}
	else if (isset($_SESSION['savedate']))
	{ 
		unset($_SESSION['savedate']);
		session_destroy();
	}

	header ("refresh:2; url=index.html");
?>