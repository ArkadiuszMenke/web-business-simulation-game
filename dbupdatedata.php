<html>
	<head>
		<style>

		body
		{
			background: #282828;
			margin: 0 auto;
			width:300px;
			color:#ff8c00;
			font-size: 20;
		}

		</style>
	</head>

	<?php

		//Connecting to my database (MYSQL included in XAMPP)
		$host = "localhost";
		$user = "root";
		$pass = "";
		$dbname = "usersdb";

		$conn = mysqli_connect($host, $user, $pass, $dbname) or die("Connection Failed!");

		$UserID = $_POST['userid'];
		$SaveData = $_POST['savedata'];
		$CurrentDate = $_POST['timestampdate'];
		$CurrentTime = $_POST['timestamptime'];
		$ReplaceDate = $_POST['overwritedate'];
		$ReplaceTime = $_POST['overwritetime'];
		$Score = $_POST['score'];

		$LoadData = json_encode($SaveData);

		//New Timestamp for db
		$NewTimestamp = $CurrentDate." ".$CurrentTime;
		//Timestamp used as a reference in db
		$ReplaceTimestamp = $ReplaceDate." ".$ReplaceTime;

		function OverwriteSlot($conn,$UserID,$SaveData,$NewTimestamp,$ReplaceTimestamp)
		{
			//Update save slot query
			$sql = "UPDATE saves_tbl SET SaveData = '$SaveData', Date = '$NewTimestamp' WHERE UserID = '$UserID' AND Date = '$ReplaceTimestamp'";

			if(!mysqli_query($conn,$sql))
			{
				echo "Saving Error!";
				header ("refresh:1 url=simulation.php");
			}
			else
			{
				echo "Saved - ";
			}
		}
		//Checking if overwriting the selected field affects a highscore slot which cannot be overwritten by unfinished games or games with worse scores 
		function OverwriteSlotCheck($conn,$UserID,$NewTimestamp,$ReplaceTimestamp,$SaveData,$LoadData,$Score)
		{
			//Checking for user in scores_tbl and gathering ID plus highscore
			$sql = "SELECT SaveID, GameWeeks FROM scores_tbl WHERE UserID = '$UserID'";

			$result = $conn->query($sql);

			if ($result->num_rows > 0)
			{
				while($row = $result->fetch_assoc())
				{
					$HighscoreID = $row['SaveID'];
					$Highscore = $row["GameWeeks"];
				}

				//Getting ID of slot being overwritten
				$sql = "SELECT ID FROM saves_tbl WHERE UserID = '$UserID' AND Date = '$ReplaceTimestamp'";

				$result = $conn->query($sql);

				if ($result->num_rows > 0)
				{
					while($row = $result->fetch_assoc())
					{
						$ReplaceSaveID = $row['ID'];
					}
					
					//Comparing both save ID's to check whether slot contains highscore
					if ($ReplaceSaveID == $HighscoreID)
					{
						//Checking whether this is a finished game(only finished games can overwrite highscore)
						if ($Score == "")
						{
							echo "Regular save cannot overwrite highscore";
							$_SESSION['savedata'] = $LoadData;
							header ("refresh:1 url=simulation.php");
						}
						//Comparing scores to see if the new score is an improvement
						else if ($Score < $Highscore)
						{
							OverwriteSlot($conn,$UserID,$SaveData,$NewTimestamp,$ReplaceTimestamp);
						}
						else
						{
							echo "No improvement over highscore";
						}
					}
					else
					{
						OverwriteSlot($conn,$UserID,$SaveData,$NewTimestamp,$ReplaceTimestamp);
					}

				}
				else
				{

				}
			}
			else
			{
				OverwriteSlot($conn,$UserID,$SaveData,$NewTimestamp,$ReplaceTimestamp);
			}
		}

		function LeaderboardCheck($conn,$UserID,$NewTimestamp,$Score,$SaveData,$LoadData){
		{	
			//Getting save ID of new save
			$sql = "SELECT ID FROM saves_tbl WHERE UserID = '$UserID' AND Date = '$NewTimestamp'";

			$result = $conn->query($sql);

			if ($result->num_rows > 0)
			{
				while($row = $result->fetch_assoc())
				{
					$SaveID = $row["ID"];
				}

				//Checking for user in scores_tbl table (leaderboard storage)
				$sql = "SELECT GameWeeks FROM scores_tbl WHERE UserID = '$UserID'";

				$result = $conn->query($sql);

				//If user on leaderboard
				if ($result->num_rows > 0)
				{
					while($row = $result->fetch_assoc())
					{
						$CurrentScore = $row["GameWeeks"];
					}
					//Compare both scores and see in new score is better
					if ($Score < $CurrentScore)
					{
						//if better update leaderboard
						$sql = "UPDATE scores_tbl SET SaveID = '$SaveID', GameWeeks = '$Score' WHERE UserID = '$UserID'";

						if(!mysqli_query($conn,$sql))
						{
							echo "Saving Error!";
							
							$_SESSION['savedata'] = $LoadData;
							header ("refresh:1 url=simulation.php");
						}
						else
						{
							echo "Highscore updated on leaderboard!";

							$_SESSION['savedata'] = $LoadData;
							header ("refresh:1 url=simulation.php");

						}

					}
					else
					{
						echo "Score not better than highscore!";

						$_SESSION['savedata'] = $LoadData;
						header ("refresh:1 url=simulation.php");
					}
				}
				//If not on leaderboard
				else
				{
					$sql = "INSERT INTO scores_tbl (UserID,SaveID,GameWeeks) VALUES ('$UserID','$SaveID','$Score')";

					if(!mysqli_query($conn,$sql))
					{
						echo "Leaderboard error!";

						$_SESSION['savedata'] = $LoadData;
						header ("refresh:1 url=simulation.php");
					}
					else
					{
						echo "You are now on the Leaderboard!";

						$_SESSION['savedata'] = $LoadData;
						header ("refresh:1 url=simulation.php");
					}
				}
			}
			else
			{

			}
		}


	}
		
		//Checking slot availability
		OverwriteSlotCheck($conn,$UserID,$NewTimestamp,$ReplaceTimestamp,$SaveData,$LoadData,$Score);

		//Checking if save qualifies for leaderboard (needs to be final save of that game)
		try
		{
			
			if ($Score != "")
			{
				LeaderboardCheck($conn,$UserID,$NewTimestamp,$Score,$SaveData,$LoadData);	
			}
			else
			{
				throw new Exception();
			}
		}
		catch (Exception $e)
		{

		}
		header ("refresh:1 url=simulation.php");
	?>
<html>