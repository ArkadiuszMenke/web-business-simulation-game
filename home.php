<!DOCTYPE html>
<html>
	<head>
		<style>

		body 
		{
			background-color: #282828;
		}
		
		ul 
		{
		  list-style-type: none;
		  margin: 0;
		  padding: 0;
		  overflow: hidden;
		  background-color: #333;
		}

		li 
		{
		  float: left;
		  border-right:1px solid #bbb;
		}

		li a 
		{
		  display: block;
		  color: #ff8c00;
		  text-align: center;
		  padding: 14px 16px;
		  text-decoration: none;
		}

		table 
		{
		  height: 30%;
		  width: 50%;
		  position: fixed;
		  z-index: 1;
		  top: 80;
		  overflow-x: hidden;
		  border-style: solid;
		  border-width: 10px;
		  border-color: #111;
		  padding-top: 20px;
		  background:#111;
			
		}

		th
		{
			font-size:25px;
			border-bottom:solid;
			border-color:#ff8c00;
			color:#ff8c00;

		}

		td
		{
			border-color:#ff8c00;
			color:#ff8c00;
			font-size:20px;
			text-align:center;
			background:#282828;


		}

		li a:hover 
		{
		  background-color: #111;
		}

		input
		{
		  font-size: 20px;  
		  border: none;
		  outline: none;
		  color: #ff8c00;
		  padding: 14px 16px;
		  background-color: inherit;
		  font-family: inherit;
		  margin: 0;
		}

		input:hover
		{
		  color:green;
		}



		.dropdown 
		{
		  float: right;
		  overflow: hidden;
		}

		.dropdown .UserTab {
		  font-size: 16px;  
		  border: none;
		  outline: none;
		  color: #ff8c00;
		  padding: 14px 16px;
		  background-color: inherit;
		  font-family: inherit;
		  margin: 0;
		}

		.dropdown:hover .UserTab {
		  background-color: #111;
		}

		.dropdown-content {
		  display: none;
		  position: absolute;
		  border-style: solid;
		  border-color: #333;
		  right: 0;
		  background-color: #222;
		  min-width: 160px;
		  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		  z-index: 1;
		}

		.dropdown-content a {
		  float: none;
		  color: #ff8c00;
		  padding: 12px 16px;
		  text-decoration: none;
		  display: block;
		  text-align: left;
		}

		.dropdown-content a:hover {
		  background-color: #333;
		}

		.dropdown:hover .dropdown-content {
		  display: block;
		}

		.save_header
		{
			color:#ff8c00;
			padding-left:20%;
			font-size: 30px;
		}

		.leaderboard_section
		{
			margin-top:250px
		}
		.leaderboard_header
		{
			color:#ff8c00;
			padding-left:20%;
			font-size: 30px;
			padding-top: 100px;

		}

		.NewSim
		{
			color:#ff8c00;
			font-size:30px;
			text-decoration:none;
			border:solid;
			border-color:#111;
			border-width:thick;
			background:#111;
			float: right;
			padding: 10% 5%;
				
				
		}

		
		
		</style>

	</head>
	<body>
		<?php 
			session_start();
			
			//Checks whether a user is logged in
			if(!isset($_SESSION['logged_in']))
			{ 
				//if not then redirect back to login page
      			header ("location:index.html");
      		}
		?>
		<nav>
			<ul>
			  <li><a href="home.php">Home</a></li>
			  <div class="dropdown">
			    <button id="AccountTab" class="UserTab" name="usernamebox">Account 
			      <i class="fa fa-caret-down"></i>
			    </button>
			    <div class="dropdown-content">
			      <a id="Logout_btn" href="logout.php">Logout</a>
			    </div>
			  </div> 
			
			</ul>
		</nav>

		<section>
			<h2 class="save_header">Save Data</h2>
		
			<?php

			//Connecting to my database (PHPMyAdmin included in XAMPP)
				//Variables to use in command
				$host = "localhost";
				$user = "root";
				$pass = "";
				$dbname = "usersdb";

				//Command used to connect to connect to database
				$conn = mysqli_connect($host, $user, $pass, $dbname) or die("Connection Failed!");
				
				//Getting username from session storage
				$Username = $_SESSION['logged_in'];

				//Query used to select date and username of a save file
				$sql = "SELECT saves_tbl.Date, logins_tbl.Username FROM saves_tbl INNER JOIN logins_tbl ON saves_tbl.UserID = logins_tbl.ID WHERE logins_tbl.Username = '$Username'";

				$result = $conn->query($sql);

				//Creating table to display saves
				echo "<table>";
				echo "<tr><th>Save</th><th>Date</th><th>Created by</th><th></th><th></th></tr>";

				$SaveCount = 0;
				while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
					//Variables to be used when creating html in php
					$SaveCount += 1;
					$SaveName = "Savename".$SaveCount;
					$DeleteName = "Deletename".$SaveCount;
					$SavedateId = "Savedate".$SaveCount;
					$FormLoadId = "FormLoad".$SaveCount;
					$FormDeleteId = "FormDelete".$SaveCount;
					$SaveId = "SaveId".$SaveCount;
					$DeleteId = "DeleteId".$SaveCount;
					$LoadDestination = "dbloaddata.php";
					$DeleteDestination = "dbdeletedata.php";
					$PreLoadScript = "Load".$SaveCount."()";
					$PreDeleteScript = "Delete".$SaveCount."()";
					$TypeHidden = "hidden";
					$TypeSubmit = "submit";
					$LoadValueSubmit = "Load";
					$DeleteValueSubmit = "Delete";
					$ValueData = "";
					$Method = "post";

					//Creating text fields
					echo "<tr><td>";
					echo $SaveCount;
					echo "</td><td id=$SavedateId>";
					echo $row['Date'];
					echo "</td><td>";
					echo $row['Username'];
					echo "</td><td>";

					//Creating load button
					echo "<form name=$FormLoadId action=$LoadDestination onsubmit=$PreLoadScript method=$Method>";
					echo "<input type=$TypeHidden id=$SaveId name=$SaveName value=$ValueData>";
					echo "<input type=$TypeSubmit value=$LoadValueSubmit>";
					echo "</form>";
					echo "</td><td>";

					//Creating Delete button
					echo "<form name=$FormDeleteId action=$DeleteDestination onsubmit=$PreDeleteScript method=$Method>";
					echo "<input type=$TypeHidden id=$DeleteId name=$DeleteName value=$ValueData>";
					echo "<input type=$TypeSubmit value=$DeleteValueSubmit>";
					echo "</form>";
					echo "</td></tr>";
					
				}

				echo "</table>";


				$conn->close();
			?>
		
		</section>

		<section>
			<a id="NewSim_btn" class="NewSim" href="simulation.php" onclick="NewSimulation()">Start New Simulation</a>
		</section>

		<section class="leaderboard_section">
			<h2 class="leaderboard_header">Leaderboard</h2>
			<?php
				$host = "localhost";
				$user = "root";
				$pass = "";
				$dbname = "usersdb";

				//Command used to connect to connect to database
				$conn = mysqli_connect($host, $user, $pass, $dbname) or die("Connection Failed!");
				
				$sql = "SELECT scores_tbl.GameWeeks, logins_tbl.Username, saves_tbl.Date FROM scores_tbl INNER JOIN logins_tbl ON scores_tbl.UserID = logins_tbl.ID INNER JOIN saves_tbl ON scores_tbl.SaveID = saves_tbl.ID ORDER BY GameWeeks * 1 ASC LIMIT 5";

				$result = $conn->query($sql);

				//Creating table to display saves
				echo "<table>";
				echo "<tr><th>Weeks</th><th>Username</th><th>Date</th></tr>";

				$HighScores = 0;
				while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
					$HighScores += 1;
					
					echo "<tr><td>";
					echo $row['GameWeeks'];
					echo "</td><td>";
					echo $row['Username'];
					echo "</td><td>";
					echo $row['Date'];
					echo "</td>";				
				}

				echo "</table>";


				$conn->close();
			?>
		</section>


		

		<script>
			//User config
			var Username = sessionStorage.getItem("user");
			AccountTab.innerHTML = Username;

			//Logging out
			LOGOUT = document.getElementById("Logout_btn");
			LOGOUT.onclick = function()
			{
				sessionStorage.clear();

				alert("For security, please restart browser");
			};

			//Function used when starting a new simulation
			function NewSimulation()
			{
				//Session storage updated to tell simulation to start a new simulation
				sessionStorage.setItem("loadtype", "START");	
			}
			
			//Functions used when loading a simulation
			function Load1()
			{
				var SaveDate = Savedate1.innerHTML;
				document.getElementById("SaveId1").value = SaveDate;
				//Session storage updated to tell simulation to load simulation with id 1
				sessionStorage.setItem("loadtype", "LOAD");	
			}

			function Load2()
			{
				var SaveDate = Savedate2.innerHTML;
				document.getElementById("SaveId2").value = SaveDate;
				//Session storage updated to tell simulation to load simulation with id 2	
				sessionStorage.setItem("loadtype", "LOAD");	
			}

			function Load3()
			{
				var SaveDate = Savedate3.innerHTML;
				document.getElementById("SaveId3").value = SaveDate;
				//Session storage updated to tell simulation to load simulation with id 3
				sessionStorage.setItem("loadtype", "LOAD");		
			}

			//Delete Scripts
			function Delete1()
			{
				var SaveDate = Savedate1.innerHTML;
				document.getElementById("DeleteId1").value = SaveDate;
			}

			function Delete2()
			{
				var SaveDate = Savedate2.innerHTML;
				document.getElementById("DeleteId2").value = SaveDate;	
			}

			function Delete3()
			{
				var SaveDate = Savedate3.innerHTML;
				document.getElementById("DeleteId3").value = SaveDate;
			}
			


		</script>

	</body>

</html>