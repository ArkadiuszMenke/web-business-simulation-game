# Web Business Simulation Game - By Arkadiusz Menke

School project between September 2018 - spring 2019. It is a clicker type game with a working stock market and event system. Includes sign up and login system as well as password retrieval. This makes it possible to save data into a MySQL database.

-The Game-
The game is a clicker type game based off popular creations such as adventure capitalist and the paperclip game. This was a project completed in under a year by somebody who took the time to learn HTML, CSS and JavaScript from the beginning which is why it does not look optimal and crisp as modern day webpages.

The aim of the game is to make money to buy more money making industries as well as investing in the stock market and fighting off events that occur after the year 1920. To end the simulation and win, the player must obtain $1 billion in their bank at the same time.

-Login-
This simulation is set up so you can log into your account to play. This was made possible by a MySQL server hosted locally (though this would work on the internet if it was hosted) which used PHP to communicate and send data between the client and server. 

All user password data is hashed and salted to add a layer of encryption when transfering data between the client and server. The application is made so that multiple users can access the game at one time and their data is saved on seperate accounts. There is however data overlap in the leaderboard which is viewable to all users.