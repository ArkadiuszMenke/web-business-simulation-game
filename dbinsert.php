<?php

	//Connecting to my database (MYSQL included in XAMPP)
	$host = "localhost";
	$user = "root";
	$pass = "";
	$dbname = "usersdb";

	$conn = mysqli_connect($host, $user, $pass, $dbname) or die("Connection Failed!");

	//Setting variables to send
	$Email = $_POST['email'];
	$Username = $_POST['username'];
	$Password = $_POST['password'];

	//Hash function constants
	define("MODULO", 2484326083);
	define("PRIME", 2058806609);
	define("SaltString", "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");

	//Creating the hash
	function ServerHash($Password)
	{
		$AsciiTot = 0;
		
		for ($i = 0; $i < strlen($Password); $i++)
		{	
			$AsciiTot += ord($Password[$i]);

		}
		
		$AsciiTot = $AsciiTot * PRIME;

		//Taking modulus
		$HashedValue = fmod($AsciiTot,MODULO);
		
		return $HashedValue;	
	}
	//Creating the salt
	function CreateSalt($Password)
	{
		$Salt = "";
		$SaltLen = 64-strlen($Password);
		$RandChar = 0;

		for ($i = 0; $i < $SaltLen; $i++)
		{
			$RandChar = random_int(0,(strlen(SaltString)-1));
			$Salt = $Salt.SaltString[$RandChar];
		}

		return $Salt;

	}
	//Combining salt and password and sending it through hash
	//Converting hash to hex
	$Salt = CreateSalt($Password);
	$Password = $Salt.$Password;
	$Password = dechex(ServerHash($Password));


	//SQL sending Username, Password and Salt to database
	$sql = "INSERT INTO `logins_tbl` (`ID`, `Email`, `Username`, `Password`, `Salt`) VALUES (NULL, '$Email', '$Username', '$Password', '$Salt');";

	//Checking if the connection and SQL worked
	if(!mysqli_query($conn,$sql))
	{
		echo "Failed to create account!";
	}
	else
	{
		echo "Account created!";
	}

	$conn->close();
	header ("refresh:2; url=index.html");

?>